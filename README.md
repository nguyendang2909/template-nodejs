NodeJS Template
====

Template for all programs

## Cai dat phan mem
- Cai dat nodejs
- Cai dat phan mem: npm i
## Huong dan khoi dong

- Chay prepare: npm run prepare
- Chay moi truong production: npm start

## Moi truong dev
- Chay moi truong dev: npm run dev
- Check lint: npm run lint
- Check duplicate code: npm run jscpd
- Check truoc khi day len gitlab: npm run precommit
- Check cap nhat thu vien: npm run audit

## License

Copyright (c) 2020 Nguyen Dang Quynh.
