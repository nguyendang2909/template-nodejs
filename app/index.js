import loggerFactory from './lib/loggerFactory';

const logger = loggerFactory.getLogger(__filename);

(async () => {
  try {
    logger.info({ message: 'Application initialize' });
  } catch (err) {
    logger.error({ message: err.stack || err });
  }
})();
